# Edentulous morphometrics

## Description
This repository contains the TPS files to reproduce the results of article "Determination of Morphogeometric Patterns in Individuals with Total Mandibular Edentulism in the Interforaminal Region from Cone Beam Computed Tomography (CBCT) Scans: A Pilot Study".

## Authors 
Liliann Abarza, Pablo Acuña-Mardones, Cristina Sanzana, Víctor Beltrán.

## Acknowledgment
We appreciate the Implant Clinic of the School of Dentistry, Universidad de La Frontera, Temuco, Chile, for their collaboration. Moreover, we thank Prof. Pablo Navarro for collaborating on the interpretation of the statistical analysis.

## Support
Inquiries should be directed to the email [pablo.acuna@ufrontera.cl](url)
